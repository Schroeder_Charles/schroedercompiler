/** 
* to run this use java -jar on the jflex jar file followed by the name of this file
* then use javac to compile the produced file. then use java the class name and the doc
* the tokentype.java class will need public enum tokenType, then list them (keywords/syms)
* two special types, number and ID. This is a custom data structure  
*/

/* declarations */

package scanner;

%%

%public
%class ScannerJFlex /* class name for java file */
%function nextToken /* renames yylex function */
%type Token /*defines the return type. next lines for EOF return value */
%eofval{
return null;
%eofval}

/* patterns */

letter		= [A-Za-z]
word		= {letter}+
whitespace	= [ \n\t]
digit		= [0-9]
number		= {digit}+
symbol		= <>|<=|>=|:=|[;,.:\[\]()+\-=*<>/]
comment		= \{[^\}]*\}

%%

/** lexical rules
* the yytext creates a function that prints out the word
*/

{word}		{
				System.out.println("Found a word: " + yytext());
				return( new Token(yytext()));
			}
			
{whitespace}	{ /* ignore whitespace */ 
				}

{number}	{
				System.out.println("Found a number: " + yytext());
				return( new Token(Integer.parseInt(yytext()), yytext()));
			}
			
{symbol}	{
				System.out.println("Found a symbol: " + yytext());
				return(new Token(yytext()));
			}
			
{comment}	{
				/* do nothing */
			}
			
.			{
				System.out.println("Illegal character: '" + yytext() + "' found.");
			}	