/*
 * This class takes in a Syntax Tree and creates a string of generated MIPS assembly
 * code. Then outputs that string to a file. This class is instanstiated in the Compiler Main class.
 * The ProgramNode is passed in as the root, then the tree is walked, and the appropriate code
 * is added to a StringBuilder and returned. StringBuilders are used in all classes to ensure
 * no null errors occur. The StringBuilder will simply add the empty string instead, which
 * will be ignored and no error will be thrown.
 */
package codegenerator;

import java.util.ArrayList;
import java.util.Iterator;
import scanner.TokenType;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.CompoundStatementNode;
import syntaxtree.DeclarationsNode;
import syntaxtree.ExpressionNode;
import syntaxtree.IfStatementNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.SubProgramDeclarationsNode;
import syntaxtree.SubProgramNode;
import syntaxtree.SyntaxTreeNode;
import syntaxtree.TypeEnum;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import syntaxtree.WhileNode;

/**
 *
 * @author Charlie
 */
public class CodeGenerator {
    
    private int currentTRegister = 0;
    private int stackPointer = 0;
    /**
     * This class creates the formatting for the assembly code, and calls the functions which
     * interact with the nodes in the passed in ProgramNode, root.
     * @param root
     * @return 
     */
    public String CreateRootAssembly(ProgramNode root)
    {
        StringBuilder code = new StringBuilder();
        code.append( ".data\n");
        code.append(writeData(root));
        code.append( ".text\n");
        code.append( "main:\n");
        
        int tempTRegValue = this.currentTRegister;
        
        //generate compound statements
        code.append(compoundStatementsCode(root, "$s0"));
        this.currentTRegister = tempTRegValue;
        
        //generate subprogram functions
        code.append(subprogramCode(root, "$s0"));
        this.currentTRegister = tempTRegValue;
        
        //end the program
        code.append( "addi\t$v0,\t10\n");
        code.append( "syscall\n");
        return( code.toString());
    }
    /**
     * This class pulls the main statements from the root node and creates an array list of them.
     * Then it iterates across the list, passing the statements to the overloaded function writeCode.
     * @param root
     * @param reg
     * @return 
     */
    private StringBuilder compoundStatementsCode(ProgramNode root, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        CompoundStatementNode cNode = root.getMain();
        ArrayList<StatementNode> statements = cNode.getStatements();
        Iterator<StatementNode> listIter = statements.iterator();
        while(listIter.hasNext())
        {
            nodeCode.append(writeCode(listIter.next(), reg));
        }
        return nodeCode;
    }
    /**
     * This function extracts the functions from the root node and then creates an array list 
     * of them. Next, it iterates across that list and passes the function pieces to the
     * overloaded function writeCode.
     * @param root
     * @param reg
     * @return 
     */
    private StringBuilder subprogramCode(ProgramNode root, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        SubProgramDeclarationsNode spdn = root.getFunctions();
        ArrayList<SubProgramNode> subNodes = spdn.getProgramNodes();
        Iterator<SubProgramNode> listIter = subNodes.iterator();
        while(listIter.hasNext())
        {
            nodeCode.append(writeCode(listIter.next(), reg));
        }
        return nodeCode;
    }
    /**
     * This function is currently unused, but may be used for functions, procedures, or
     * other complex declarations. See the writeData function for the initial variables.
     * @param root
     * @param reg
     * @return 
     */
    private StringBuilder declarationsCode(ProgramNode root, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        DeclarationsNode dNode = root.getVariables();
        ArrayList<VariableNode> variables = dNode.getVariables();
        Iterator<VariableNode> listIter = variables.iterator();
        while(listIter.hasNext())
        {
            nodeCode.append(writeCode(listIter.next(), reg));
        }
        return nodeCode;
    }
    /**
     * This function takes in the abstract node ExpressionNode, determines what it is an
     * instance of, then casts it, and sends it to the appropriate function to write that code.
     * @param node
     * @param reg
     * @return 
     */
    private StringBuilder writeCode(ExpressionNode node, String reg) 
    {
        StringBuilder nodeCode = new StringBuilder();
        if( node instanceof OperationNode) {
            nodeCode.append(writeCode( (OperationNode)node, reg));
        }
        else if( node instanceof ValueNode) {
            nodeCode.append(writeCode( (ValueNode)node, reg));
        } 
        else if(node instanceof VariableNode) {
            nodeCode.append(writeCode((VariableNode)node, reg));
        }
        return( nodeCode);
    }
    /**
     * This function handles the writing of code for all binary and unary operations. 
     * First, it grabs the left and right side of the operation node, opNode., and allocates
     * two t registers. Then it grabs the operation associated with the opNode. Finally,
     * the if chain determines what kind of operator it is, and writes the appropriate code.
     * @param opNode
     * @param resultRegister
     * @return 
     */
    public StringBuilder writeCode( OperationNode opNode, String resultRegister)
    {
        StringBuilder code = new StringBuilder();
        ExpressionNode left = opNode.getLeft();
        String leftRegister = "$t" + currentTRegister++;
        code.append(writeCode( left, leftRegister));
        ExpressionNode right = opNode.getRight();
        String rightRegister = "$t" + currentTRegister++;
        code.append(writeCode( right, rightRegister));
        TokenType kindOfOp = opNode.getOperation();
        if( kindOfOp == TokenType.PLUS)
        {
            //if the plus operator is unary, make sure you add a zero into the left reg
            if(opNode.getLeft() == null)
            {
                code.append("addi\t").append(leftRegister).append(",\t").append("$zero,\t0\n");
            }
            // add resultregister, left, right 
            code.append("add\t").append(resultRegister).append(",\t").append(leftRegister).append(",\t").append(rightRegister).append("\n");
        }
        if( kindOfOp == TokenType.MINUS)
        {
            //if it is a unary minus (negation) make sure you add a zero into the left reg
            if(opNode.getLeft() == null)
            {
                code.append("addi\t").append(leftRegister).append(",\t").append("$zero,\t0\n");
            }
             // add resultregister, left, right 
            code.append("sub\t").append(resultRegister).append(",\t").append(leftRegister).append(",\t").append(rightRegister).append("\n");
        }
        if( kindOfOp == TokenType.ASTERISK)
        {
            code.append("mult\t").append(leftRegister).append(",\t").append(rightRegister).append("\n");
            code.append("mflo\t").append(resultRegister).append("\n");
        }
        if( kindOfOp == TokenType.LESSTHAN)
        {
            code.append("slt\t").append("$at,\t").append(leftRegister).append(",\t").append(rightRegister).append("\n");
            code.append("beq\t$at,\t$zero,\t"); //append branch name in function!!
        }
        if( kindOfOp == TokenType.GREATERTHAN)
        {
            code.append("slt\t$at,\t").append(leftRegister).append(",\t").append(rightRegister).append("\n");
            code.append("bnq\t$at,\t$zero,\t"); //append branch name in function!!
        }
        this.currentTRegister -= 2;
        return( code);
    }
    /**
     * This function handles value nodes by getting their associated attributes, whether that
     * be a number or a variable.
     * @param valNode
     * @param resultRegister
     * @return 
     */
    public StringBuilder writeCode( ValueNode valNode, String resultRegister)
    {
        String value = valNode.getAttribute();
        StringBuilder code = new StringBuilder();
        
        if(valNode.getIsIndex())
        {
            
        }
        
        
        code.append("addi\t").append(resultRegister).append(",\t$zero, ").append(value).append("\n");
        return( code);
    }
    /**
     * This function handles assignment statements. First, various different types of 
     * assignment possibilities are checked. If it is found that the assignment involves
     * arrays, then it is handed off to the arrayAssignment function. 
     * Otherwise, it gets the name of the variable
     * to be assigned, then it writes the store word (sw) command to with the proper register
     * and variable name.
     * @param asNode
     * @param reg
     * @return 
     */
    public StringBuilder writeCode( AssignmentStatementNode asNode, String reg)
    {   
        StringBuilder code = new StringBuilder();
        ExpressionNode eNode = asNode.getExpression();
        
        if(eNode instanceof ValueNode)
        {
            ValueNode vn = (ValueNode)asNode.getExpression();
            if (vn.getIsIndex())
        {
            code.append(arrayAssignment(asNode, reg));
        } 
        }
        if(asNode.getArrayAssignmentIndex() > -1)
        {
            code.append(arrayAssignment(asNode, reg));
        } 
        else
        {
            String word = asNode.getLvalue().getName();
            StringBuilder expression = writeCode(asNode.getExpression(), reg);
            code.append(expression + "sw\t" + reg + ",\t" + word + "\n");
        }
        
        return code;
    }
    /**
     * This function handles writing code for if statements. First, it passes the "test" or 
     * operation within the if statement to the ExpressionNode writeCode function. Then, it writes
     * out elseBranch which will directly follow a branch command (if this jump here). 
     * It then writes a comment to inform the user that this code will be executed if the if
     * statement is true. Then it passes the then statement to the StatementNode writeCode function.
     * Next, it creates a command to jump to a tag called endIf. An else branch is created
     * whether or not an else is stated, to ensure the jump command on line 4 of this function
     * can execute. Finally, if there is an else statement, that code will be written, and
     * the endIf: line is written to ensure the statement can be exited properly.
     * @param ifNode
     * @param reg
     * @return 
     */
    public StringBuilder writeCode(IfStatementNode ifNode, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        nodeCode.append(writeCode((ExpressionNode)ifNode.getTest(), reg));
        nodeCode.append("elseBranch\n");
        nodeCode.append("#if true branch\n");
        nodeCode.append(writeCode((StatementNode)ifNode.getThenStatement(), reg));
        nodeCode.append("j,\tendIf\n");
        nodeCode.append("elseBranch:\n");
        if(ifNode.getElseStatement() != null)
        {
            nodeCode.append(writeCode((StatementNode)ifNode.getElseStatement(), reg));
        }        
        nodeCode.append("endIf:\n");
        
        return nodeCode;
    }
    /**
     * This function handles statement nodes, checking to see whether they are an instance of
     * an assignment, if, or while node. It passes them to the appropriate writeCode function.
     * @param node
     * @param reg
     * @return 
     */
    public StringBuilder writeCode(StatementNode node, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        
        if(node instanceof AssignmentStatementNode)
        {
            nodeCode.append(writeCode((AssignmentStatementNode)node, reg));
        } else if (node instanceof IfStatementNode)
        {
            nodeCode.append(writeCode((IfStatementNode)node, reg));
        } else if (node instanceof WhileNode)
        {
            nodeCode.append(writeCode((WhileNode)node, reg));
        }
        
        return nodeCode;
    }
    /**
     * This function is called first, and handles the writing of the .data section of the asm.
     * It simply uses the getVariables function to obtain the names of the variables declared
     * at the beginning of the program, then writes them as .words in the asm.
     * @param root
     * @return 
     */
    public StringBuilder writeData(ProgramNode root)
    {
        StringBuilder nodeCode = new StringBuilder();
        DeclarationsNode dNode = root.getVariables();
        ArrayList<VariableNode> variables = dNode.getVariables();
        Iterator listIter = variables.iterator();
        while(listIter.hasNext())
        {
            VariableNode vNode = (VariableNode) listIter.next();
            nodeCode.append(vNode.getName());
            if(vNode.getSize() != 0)
            {
                nodeCode.append(":\t.word\t");
                for(int i = 0; i < vNode.getSize() -1; i++)
                {
                    nodeCode.append("0, ");
                }
                nodeCode.append("0\n");
            }else
            {
                nodeCode.append(":\t.word\t0\n");
            }
        }
        return nodeCode;
    }
    /**
     * This function handles the writing of code for variable nodes that come after the initial
     * declaration. This function uses the load word command to load the associated values
     * into the proper register for use.
     * @param vNode
     * @param reg
     * @return 
     */
    public StringBuilder writeCode(VariableNode vNode, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        
        nodeCode.append("lw\t").append(reg).append("\t").append(vNode.getName()).append("\n");
        
        
        return nodeCode;
    }
    /**
     * This function handles the writing of code for While Nodes. The WhileBegin: tag is written
     * first so that it may be jumped back to while the loop is still executing. The getCondition
     * function will return an expression node, which will get passed to the expression writeCode
     * function. This will then go to the operation node writeCode function, and output the 
     * relevant code. After the operations of the loop are written, the endWhile tag is appended
     * to the jump command at the end of the operator. The register increments to ensure that
     * the do statement code doesn't overwrite the while condition register. A jump command
     * to the beginning of the while statement is issued afterwords, and will be avoided by a
     * branch command written by the writeCode operation node function. Finally, an endWhile: tag
     * is written out to ensure the loop may be exited properly.
     * @param wNode
     * @param reg
     * @return 
     */
    public StringBuilder writeCode(WhileNode wNode, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        nodeCode.append("WhileBegin:\n");
        nodeCode.append(writeCode(wNode.getCondition(), reg));
        nodeCode.append("endWhile\n");
        reg = "$t" + currentTRegister++;
        nodeCode.append(writeCode(wNode.getDo(), reg));
        nodeCode.append("j,\tWhileBegin\n");
        nodeCode.append("endWhile:\n");
        
        return nodeCode;
    }
    /**
     * This function handles writing code for assigning values into arrays. The first line loads
     * the array's address. Next, the desired index to be assigned is loaded. Then, the index
     * is added to itself twice, thus multiplying it by 4, which converts the index into bytes.
     * Then, the index time 4 is added to the address originally loaded in, thus giving the desired
     * address. Finally, the desired value (vn.getAttribute) is stored to the proper address.
     * @param node
     * @param reg
     * @return 
     */
    public StringBuilder arrayAssignment(AssignmentStatementNode node, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        ValueNode vn = (ValueNode)node.getExpression();
        
        if(vn.getIsIndex())
        {
            nodeCode.append("Misnomer \n");
        } else
        {
            nodeCode.append("la\t").append(reg).append(",\t").append(node.getLvalue().getName()).append("\n");
            currentTRegister++;
            nodeCode.append("li\t$t").append(currentTRegister).append(",\t").append(node.getArrayAssignmentIndex());
            //quadruple the index to find the address
            nodeCode.append("\n").append("add\t$t").append(currentTRegister).append(",\t$t").append(currentTRegister).append(",\t$t").append(currentTRegister);
            nodeCode.append("\n").append("add\t$t").append(currentTRegister).append(",\t$t").append(currentTRegister).append(",\t$t").append(currentTRegister);
            currentTRegister++;
            nodeCode.append("\nadd\t$t").append(currentTRegister).append(",\t");
            currentTRegister--;
            nodeCode.append("$t").append(currentTRegister).append(",\t").append(reg).append("\n");
            currentTRegister +=2;
            nodeCode.append("li\t$t").append(currentTRegister).append(",\t").append(vn.getAttribute()).append("\n");
            nodeCode.append("sw\t$t").append(currentTRegister).append(",\t0($t").append(currentTRegister -1).append(")\n");
            currentTRegister -= 3;
        }
        return nodeCode;
    }
    
    public StringBuilder writeCode(SubProgramNode node, String reg)
    {
        StringBuilder nodeCode = new StringBuilder();
        nodeCode.append(node.getFuncName()).append(":\n");
        
        DeclarationsNode dNode = node.getDn();
        ArrayList<VariableNode> variables = dNode.getVariables();
        Iterator<VariableNode> listIter = variables.iterator();
        while(listIter.hasNext())
        {
            nodeCode.append(writeCode(listIter.next(), reg));
        }
        
        CompoundStatementNode cNode = node.getCsn();
        ArrayList<StatementNode> statements = cNode.getStatements();
        Iterator<StatementNode> listIterTwo = statements.iterator();
        while(listIterTwo.hasNext())
        {
            nodeCode.append(writeCode(listIterTwo.next(), reg));
        }
        
        
        return nodeCode;
    }
    
}
