/*
 * Use this main to run (create and link) the various pieces of the compiler.
 */
package compiler;

import codegenerator.CodeGenerator;
import java.io.IOException;
import java.io.PrintWriter;
import parser.Parser;
import scanner.main;
import syntaxtree.ProgramNode;
import syntaxtree.SyntaxTreeNode;

/**
 * This is the main function that is called on the command line when unjarred.
 * This class initializes a new parser which generates a symbol table and 
 * syntax tree. The tree is then passed to the code generation class. 
 * Finally, the generated code is written to a file.
 * @author Charlie
 */
public class CompilerMain {
    
    public static void main(String[] args) {
        //a string containing the pascal file to be compiled
        String filename = args[0];
        //This creates a new instance of a parser, the boolean indicates whether it is a file or string
        Parser p = new Parser( filename, true);
        //this instantiates a new syntax tree
        ProgramNode instance = p.program();
        //this prints out the symbol table to the console
        p.getSymbolTable();
        //send instance to code generator
        CodeGenerator codeGen = new CodeGenerator();
    
        String generatedCode = codeGen.CreateRootAssembly(instance);
        //this prints out the syntax tree and generated code
       System.out.println(instance.indentedToString(0));
       System.out.println(generatedCode);
       //this writes the produced assembly to a file. 
       try{
           PrintWriter writer = new PrintWriter("output.asm", "UTF-8");
           writer.println(generatedCode);
           writer.close();
       } catch (IOException e)
         {
              //do nothing     
         }
        
    }
}
