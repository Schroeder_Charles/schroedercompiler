/*
 * This is a parser that will pass tokens through the functions and create a syntax tree, and 
 * symbol table. It can be instantiated and called on a file or a string. Pass in the boolean as
 * true if it's a filename, or false if it's a string.
 */
package parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import scanner.LookUpTable;
import scanner.Token;
import scanner.TokenType;
import scanner.ScannerJFlex;
import symboltable.SymbolTable;
import symboltable.SymbolTable.IdKind;
import static symboltable.SymbolTable.IdKind.FUNCTION_NAME;
import static symboltable.SymbolTable.IdKind.PROCEDURE_NAME;
import static symboltable.SymbolTable.IdKind.PROGRAM_NAME;
import static symboltable.SymbolTable.IdKind.VARIABLE_NAME;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.CompoundStatementNode;
import syntaxtree.DeclarationsNode;
import syntaxtree.ExpressionListNode;
import syntaxtree.ExpressionNode;
import syntaxtree.IfStatementNode;
import syntaxtree.OperationNode;
import syntaxtree.ProcedureStatementNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.SubProgramDeclarationsNode;
import syntaxtree.SubProgramNode;
import syntaxtree.TypeEnum;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;
import syntaxtree.WhileNode;

/**
 *
 * @author Charlie Schroeder
 */
public class Parser {
    
    //Instance Variables
    private Token lookAhead;
    private ScannerJFlex scanner;
    private SymbolTable st = new SymbolTable();
    private int arraySize = 0;
    private int arrayAssignmentIndex = -1;
    private boolean isArrayAssignment = false;
    
    //constructors
    
    /**
     * This function initializes the parser and begins the process of iterating through the list
     * of incoming tokens.
     * @param text
     * @param isFileName 
     */
    public Parser(String text, boolean isFileName)
    {
        if(isFileName)
        {
            FileInputStream fis = null;
        
            try
            {
                fis = new FileInputStream(text);
            } catch (FileNotFoundException ex) 
            {
                error("No such file");
            }
            InputStreamReader isr = new InputStreamReader(fis);
            scanner = new ScannerJFlex(isr);
        } else
        {
            scanner = new ScannerJFlex( new StringReader( text));
        }
        
        try
        {
            lookAhead = scanner.nextToken();
        } catch (IOException ex) 
        {
            error("Could not get next token");
        }
    }
     /*
                                  T H E
                           *******************
                          / | M E T H O D S | \
                         ***********************
                                  ! ! !
    */
    /** The match function matches the found token type to the correct token type. 
    */
    public void match(TokenType expected)
    {
        if(this.lookAhead.getType() == expected)
        {
            try
            {
                this.lookAhead = scanner.nextToken();
                if( this.lookAhead == null) {
                    this.lookAhead = new Token("EOF", null);
                } 
        } catch (IOException ex)
                {
                    error("cannot get next token");
                }
        }
        else {
            error("Invalid Match. Expected " + expected + " received " + this.lookAhead.getType());
        }
    }
    
    public void error(String message)
    {
        System.out.println("Error: " + message);
        System.exit(1); 
    }
    
    public ProgramNode program()
    {
       
        match(TokenType.PROGRAM);
        String programName = lookAhead.getLexeme();
        match(TokenType.ID);
        st.add(programName, PROGRAM_NAME);
        ProgramNode programNode = new ProgramNode(programName);
        DeclarationsNode dn = new DeclarationsNode();
        SubProgramDeclarationsNode spdn = new SubProgramDeclarationsNode();
        CompoundStatementNode csn = new CompoundStatementNode();
        
        match(TokenType.SEMICOLON);
        programNode.setVariables(declarations());
        programNode.setFunctions(subprogram_declarations());
        programNode.setMain(compound_statement());
        match(TokenType.PERIOD);
        
        return programNode;
                   
    }
    
    public ArrayList<VariableNode> identifier_list()
    {
        ArrayList<VariableNode> vList = new ArrayList();
        String variableName = lookAhead.getLexeme();
        match(TokenType.ID);
        st.add(variableName, VARIABLE_NAME);
        VariableNode vNode = new VariableNode(variableName, 0);
        vList.add(vNode);
            
        while(lookAhead.getType() == TokenType.COMMA)
        {
            match(TokenType.COMMA);
            variableName = lookAhead.getLexeme();
            match(TokenType.ID);
            st.add(variableName, VARIABLE_NAME);
            vNode = new VariableNode(variableName, 0);
            vList.add(vNode);
        }
        
        return vList;
    }
    
    public DeclarationsNode declarations()
    {   //TODO fix return if no declarations exist
        // this might want to get strings from indentifier list, and type from type
        //then create the variable
        
        DeclarationsNode dn = new DeclarationsNode();
        
        while(lookAhead.getType() == TokenType.VAR)
        {
            match(TokenType.VAR);
            ArrayList<VariableNode> vList = identifier_list();
           
            match(TokenType.COLON);
            TypeEnum tempType = type(); 
            
            //walk through node list and anyone without a type gets this type
            for(VariableNode vn : vList)
            {
                vn.setType(tempType);
                String varName = vn.getName();
                st.setType(varName, tempType);
                dn.addVariable(vn);
                
                if(tempType == TypeEnum.INTEGERARRAY || tempType == TypeEnum.REALARRAY)
                {
                    vn.setSize(arraySize);
                    //st.setSize(varName, arraySize);
                }
            }
            
            arraySize = 0;   
            match(TokenType.SEMICOLON);
                       
        }
        return dn;
    }
    
    public TypeEnum type()
    {
        TypeEnum answer = null;
        
        if(lookAhead.getType() == TokenType.ARRAY)
        {
            match(TokenType.ARRAY);   
            arraySize = array();

            answer = standard_type();
            
            if(answer == TypeEnum.INTEGER)
            {
                answer = TypeEnum.INTEGERARRAY;
            } else if (answer == TypeEnum.REAL)
            {
                answer = TypeEnum.REALARRAY;
            }
            
        } else
        {
            answer = standard_type();
        }
        return answer;
    }
    
    public TypeEnum standard_type()
    {
        TypeEnum answer = null;
        
        if(lookAhead.getType() == TokenType.INTEGER)
        {
            match(TokenType.INTEGER);
            answer = TypeEnum.INTEGER;
        } else if (lookAhead.getType() == TokenType.REAL)
        {
            match(TokenType.REAL);
            answer = TypeEnum.REAL;
        }
        
        return answer;
    }
    
    public SubProgramDeclarationsNode subprogram_declarations()
    {
        SubProgramDeclarationsNode answer = new SubProgramDeclarationsNode();
        while(lookAhead.getType() == TokenType.FUNCTION) //or procedure
        {
            answer.addSubProgramDeclaration(subprogram_declaration());
            match(TokenType.SEMICOLON);
        } 
        
        return answer;
    }
    
    public SubProgramNode subprogram_declaration()
    {
        SubProgramNode answer = new SubProgramNode();
        subprogram_head(answer);
        DeclarationsNode dNode = declarations();
        SubProgramDeclarationsNode sp = subprogram_declarations();
        CompoundStatementNode csNode = compound_statement();
        answer.setCsn(csNode);
        answer.setDn(dNode);
        answer.setSp(sp);
        
        return answer;
    }
    
    public void subprogram_head(SubProgramNode sNode)
    {
        if(lookAhead.getType() == TokenType.FUNCTION)
        {
            match(TokenType.FUNCTION);            
            String pn = lookAhead.getLexeme();
            match(TokenType.ID);
            sNode.setFuncName(pn);
            st.add(pn, FUNCTION_NAME);
            sNode.setDn(arguments());
            match(TokenType.COLON);
            sNode.setReturnType(standard_type());
            match(TokenType.SEMICOLON);
          
        } else if(lookAhead.getType() == TokenType.PROCEDURE)
        {
            match(TokenType.PROCEDURE);
            String pn = lookAhead.getLexeme();
            match(TokenType.ID);
            st.add(pn, PROCEDURE_NAME); 
            
            sNode.setDn(arguments());   
            match(TokenType.SEMICOLON);
        }
    }
    
    public DeclarationsNode arguments()
    {
        DeclarationsNode dNode = new DeclarationsNode();
        
        if(lookAhead.getType() == TokenType.LEFTPARANTHESIS)
        {
            match(TokenType.LEFTPARANTHESIS);
            dNode = parameter_list();
            match(TokenType.RIGHTPARANTHESIS);
        } else
        {
            //lambda
        }
        return dNode;
    }
    
    public DeclarationsNode parameter_list()
    {
        DeclarationsNode dNode = new DeclarationsNode();
        
        ArrayList<VariableNode> nodeList = identifier_list();
        
        match(TokenType.COLON);
            
        TypeEnum tempType = type();
        
        for(VariableNode node : nodeList)
        {
            node.setType(tempType);
            String varName = node.getName();
            st.setType(varName, tempType);
            dNode.addVariable(node);
        }
            
            while(lookAhead.getType() == TokenType.SEMICOLON)
            {
                match(TokenType.SEMICOLON);
                nodeList = identifier_list();
                match(TokenType.COLON);
                tempType = type();
                
                for(VariableNode node : nodeList)
                {
                    node.setType(tempType);
                    String varName = node.getName();
                    st.setType(varName, tempType);
                    dNode.addVariable(node);
                }
            }
            
        return dNode;
        
    }
    
    public CompoundStatementNode compound_statement()
    { 
        CompoundStatementNode csn = new CompoundStatementNode();
        
        match(TokenType.BEGIN);
        ArrayList<StatementNode> nodeList = new ArrayList(optional_statements());
        for(StatementNode sn : nodeList)
        {
            csn.addStatement(sn);
        }
        match(TokenType.END); 
        
        return csn;
    }
    
    public ArrayList<StatementNode> optional_statements()
    {
        ArrayList<StatementNode> nodeList = new ArrayList();
        
        if(lookAhead.getType() == TokenType.END)
        {
            //lambda don't match, compound_statement function list does that
        } else
        {
            nodeList = statement_list();
        }
        
        return nodeList;
    }
    
    public ArrayList<StatementNode> statement_list()
    {
        ArrayList<StatementNode> nodeList = new ArrayList();
        StatementNode sn = statement();
        nodeList.add(sn);
        
        while(lookAhead.getType() == TokenType.SEMICOLON)
        {
            match(TokenType.SEMICOLON);
            sn = statement();
            nodeList.add(sn);
        }
        return nodeList;
    }
    
    public StatementNode statement()
    {   
        StatementNode statementNode = null;
        
        if(lookAhead.getType() == TokenType.ID)
        {
            if(st.findKind(lookAhead.toString()) == VARIABLE_NAME)
            {
                AssignmentStatementNode asNode = new AssignmentStatementNode();
                asNode.setLvalue(variable());
                if(isArrayAssignment)
                {
                    asNode.setArrayAssignmentIndex(arrayAssignmentIndex);
                }
                arrayAssignmentIndex = -1;
                isArrayAssignment = false;
                match(TokenType.COLONEQUALS);
                
                //semantic analysis
                String varName = asNode.getLvalue().toString();
                checkAssignment(varName);
                
                if(!st.exists(varName))
                {
                    error("variable unassigned\n");
                }
                
                asNode.setExpression(expression());
                statementNode = asNode;
 
            } else if(st.findKind(lookAhead.toString()) == PROCEDURE_NAME)
            {
                statementNode = procedure_statement(); //use the new node
            }
        } else if (lookAhead.getType() == TokenType.IF)
        {
            match(TokenType.IF);
            IfStatementNode ifs = new IfStatementNode();
            ifs.setTest(expression());
            
                match(TokenType.THEN);
                ifs.setThenStatement(statement());
                
                    match(TokenType.ELSE);
                    ifs.setElseStatement(statement());
            
                statementNode = ifs;
            
        } else if (lookAhead.getType() == TokenType.WHILE)
        {
            match(TokenType.WHILE);
            WhileNode wn = new WhileNode();
            wn.setCondition(expression());
            statementNode = wn;
            if(lookAhead.getType() == TokenType.DO)
            {
                match(TokenType.DO);
                wn.setDo(statement());
                statementNode = wn;
            }
        } else if (lookAhead.getType() == TokenType.BEGIN)
        {
           match(TokenType.BEGIN);
           statementNode = compound_statement();
        }
        
        //csn.addStatement(statementNode);
        return statementNode;
    }
    
    public VariableNode variable()
    {
        String pn = lookAhead.getLexeme();
        match(TokenType.ID);
   
        st.add(pn, VARIABLE_NAME);
           
        
        VariableNode vNode = new VariableNode(pn,0);
        
        if(lookAhead.getType() == TokenType.LEFTSQUAREBRACKET)
        {
            match(TokenType.LEFTSQUAREBRACKET);
            isArrayAssignment = true;
            expression();
            match(TokenType.RIGHTSQUAREBRACKET);
        } else
        {
            //do nothing
        }
        
        return vNode;
    }
    
    public StatementNode procedure_statement()
    {
        String pn = lookAhead.getLexeme();
        match(TokenType.ID);
       
        st.add(pn, PROCEDURE_NAME);
        ProcedureStatementNode psn = null;
        psn.setName(pn);
            
        if(lookAhead.getType() == TokenType.LEFTPARANTHESIS)
        {
            match(TokenType.LEFTPARANTHESIS);
            psn.setArguments(expression_list());
            match(TokenType.RIGHTPARANTHESIS);
        }
        StatementNode sn = psn;
        return psn;
    }
    
    public ArrayList expression_list()
    { //returns a list of expression nodes. create a new array list
        ArrayList<ExpressionNode> answer = new ArrayList();
        ExpressionNode node = expression();
        answer.add(node);
        
        while(lookAhead.getType() == TokenType.COMMA)
        {
            match(TokenType.COMMA);
            node = expression();
            answer.add(node);
        }
        
        return answer;
        
    }
    
    public ExpressionNode expression()
    {
        ExpressionNode left = null;
        ExpressionNode eNode = null;
        left = simple_expression(left);
        eNode = left;
        
        OperationNode relexpression = relop();
        
        if(relexpression != null)
        {
          ExpressionNode right = simple_expression(left);
          relexpression.setLeft(left);
          relexpression.setRight(right);
          eNode = relexpression;
        }
        return eNode;
    }
    
    public ExpressionNode simple_expression(ExpressionNode left)
    {
           
        if(lookAhead.getType() == TokenType.PLUS || lookAhead.getType() == TokenType.MINUS)
        {
           OperationNode oNode = sign();
           ExpressionNode right = term();
            
            oNode.setLeft(left);
            oNode.setRight(right);
            
            return simple_part(oNode);
            
        } else
        {
            left = term();
            ExpressionNode possibleLeft = simple_part(left);
            if(possibleLeft != null)
            {
                return possibleLeft;
            }
            else
            {
                return left;
            }
        }
        
    }
    
    public ExpressionNode simple_part(ExpressionNode left)
    {
        ExpressionNode right = null;
        
        if(lookAhead.getType() == TokenType.PLUS)
        {
            match(TokenType.PLUS);
            OperationNode oNode = new OperationNode(TokenType.PLUS);
            right = term();
            
                oNode.setLeft(left);
            
                oNode.setRight(right);
            
            simple_part(left);
            left = oNode;
        } else if (lookAhead.getType() == TokenType.MINUS)
        {
            match(TokenType.MINUS);
            OperationNode oNode = new OperationNode(TokenType.MINUS);
            right = term();
            
                oNode.setLeft(left);
            
                oNode.setRight(right);
            
            simple_part(left);
            left = oNode;
        } else if (lookAhead.getType() == TokenType.OR)
        {
            match(TokenType.OR);
            OperationNode oNode = new OperationNode(TokenType.OR);
            right = term();
            
                oNode.setLeft(left);
        
                oNode.setRight(right);
            
            simple_part(left);
            left = oNode;
        } else
        {
            //lambda
        }
            return left;
    }
    
    public ExpressionNode term()
    {
        ExpressionNode left = factor();
        return term_part(left);
    }
    
    public ExpressionNode term_part(ExpressionNode left)
    {
        
        if(isMulop(lookAhead))
        {
            OperationNode oNode = mulop();
            ExpressionNode right = factor();
            oNode.setRight(right);
            oNode.setLeft(left);
            return term_part(oNode);
        } else
        {
            //lambda
            return left;
        }
       
    }
    
    public ExpressionNode factor()
    {
        ExpressionNode answer = null;
        
        if(lookAhead.getType() == TokenType.ID)
        {
            String pn = lookAhead.getLexeme();
            match(TokenType.ID);
         
            st.add(pn, VARIABLE_NAME);
            answer = new VariableNode(pn,0);
            answer.setType(TypeEnum.VARIABLE);
            
            if(lookAhead.getType() == TokenType.LEFTSQUAREBRACKET)
            {
                match(TokenType.LEFTSQUAREBRACKET);
                answer = expression();
                match(TokenType.RIGHTSQUAREBRACKET);
            } else if(lookAhead.getType() == TokenType.LEFTPARANTHESIS)
            {
                match(TokenType.LEFTPARANTHESIS);
                expression_list();
                match(TokenType.RIGHTPARANTHESIS);
            } else
            {
                //do nothing
            }
        } else if (lookAhead.getType() == TokenType.NUM)
        {
            Token valueToken = this.lookAhead;
            arrayAssignmentIndex = lookAhead.getValue();
            match(TokenType.NUM);
            
            if(isArrayAssignment)
            {
                ValueNode vn = new ValueNode(valueToken.getLexeme());
                vn.setIsIndex();
                answer = vn;
            } else
            {
                answer = new ValueNode(valueToken.getLexeme());
            }
            
            answer.setType(TypeEnum.INTEGER);
            
        } else if(lookAhead.getType() == TokenType.LEFTPARANTHESIS)
        {
            match(TokenType.LEFTPARANTHESIS);
            expression();
            match(TokenType.RIGHTPARANTHESIS);
        } else if (lookAhead.getType() == TokenType.NOT)
        {
            match(TokenType.NOT);
            factor();
        } else
        {
            error("factor");
        }
        
        return answer;
    }
    
    public OperationNode sign()
    {
        OperationNode answer = null;
        
        if(lookAhead.getType() == TokenType.PLUS)
        {
            match(TokenType.PLUS);
            answer = new OperationNode(TokenType.PLUS);
        } else if (lookAhead.getType() == TokenType.MINUS)
        {
            match(TokenType.MINUS);
            answer = new OperationNode(TokenType.MINUS);
        } 
        return answer;
    }
    
    public int array()
    {
        match(TokenType.LEFTSQUAREBRACKET);
        match(TokenType.NUM);
        match(TokenType.COLON);
        int size = lookAhead.getValue();
        match(TokenType.NUM);
        match(TokenType.RIGHTSQUAREBRACKET);
        match(TokenType.OF);
        return size;
    }
    
    public OperationNode mulop()
    {
        OperationNode oNode = null;
        
        if(lookAhead.getType() == TokenType.ASTERISK)
        {
            match(TokenType.ASTERISK);
            oNode = new OperationNode(TokenType.ASTERISK);
            
        } else if(lookAhead.getType() == TokenType.FORWARDSLASH)
        {
            match(TokenType.FORWARDSLASH);
            oNode= new OperationNode(TokenType.FORWARDSLASH);
            
        } else if(lookAhead.getType() == TokenType.DIV)
        {
            match(TokenType.DIV);
            oNode = new OperationNode(TokenType.DIV);
            
        } else if(lookAhead.getType() == TokenType.MOD)
        {
            match(TokenType.MOD);
            oNode = new OperationNode(TokenType.MOD);
            
        } else if(lookAhead.getType() == TokenType.AND)
        {
            match(TokenType.AND);
            oNode = new OperationNode(TokenType.AND);
            
        } else
        {
            error("Mulop");
        }
        return oNode;
    }
    
    public boolean isMulop(Token token)
    {
        boolean answer = false;
        
        if(token.getType() == TokenType.ASTERISK || token.getType() == TokenType.DIV
                || token.getType() == TokenType.FORWARDSLASH || token.getType() == TokenType.MOD
                || token.getType() == TokenType.AND)
        {
            answer = true;
        }
        return answer;
    }
    
    public OperationNode relop()
    {
        OperationNode Relop = null;
        if(lookAhead.getType() == TokenType.EQUALS)
        {
            Relop = new OperationNode(TokenType.EQUALS);
            match(TokenType.EQUALS);
            
        } else if(lookAhead.getType() == TokenType.DIAMOND)
        {
            Relop = new OperationNode(TokenType.DIAMOND);
            match(TokenType.DIAMOND);
            
        } else if (lookAhead.getType() == TokenType.LESSTHAN)
        {
           
            Relop = new OperationNode(TokenType.LESSTHAN);
            match(TokenType.LESSTHAN);
           
        } else if (lookAhead.getType() == TokenType.GREATERTHAN)
        {
            
            Relop = new OperationNode(TokenType.GREATERTHAN);
            match(TokenType.GREATERTHAN);
            
        } else if (lookAhead.getType() == TokenType.LESSTHANEQUALS)
        {
           
            Relop = new OperationNode(TokenType.LESSTHANEQUALS);
            match(TokenType.LESSTHANEQUALS);
          
        } else if (lookAhead.getType() == TokenType.GREATERTHANEQUALS)
        {
           
            Relop = new OperationNode(TokenType.GREATERTHANEQUALS);
            match(TokenType.GREATERTHANEQUALS);
            
        } 
         return Relop;
    }
    
    /**
     * This function calls the output function in the symbol table which will print the table
     * to the command line.
     */
    public void getSymbolTable() 
    {
        st.outputST();
    }
    
    /**
     * This function makes sure variables and arrays are assigned correctly.
     * @param varName 
     */
    private void checkAssignment(String varName) 
    {
        TypeEnum lookAheadType = null;
        
        if(lookAhead.getType() == TokenType.INTEGER)
        {
            lookAheadType = TypeEnum.INTEGER;
        } else if (lookAhead.getType() == TokenType.REAL)
        {
            lookAheadType = TypeEnum.REAL;
        }
        
        if(lookAheadType != null && lookAheadType != st.getType(varName))
        {
          error("bad assignment");   
        }
    }
    
}