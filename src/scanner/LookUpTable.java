/*
 * This hashmap matches the enum types with the string form of the symbol/keyword.
 */
package scanner;

import java.util.HashMap;
/**
 *
 * @author Charlie Schroeder
 */
public class LookUpTable {
    
    
    
   public static HashMap<String, TokenType> lookUp;
   
   /**
    * This sets up the hashmap and adds all the symbols included in the language to it.
    * The symbols are added manually to ensure ease of access upon adding new symbols.
    */
   static 
   {
        lookUp = new HashMap<>();
		
       lookUp.put("and", TokenType.AND);
       lookUp.put("array", TokenType.ARRAY);
       lookUp.put("begin", TokenType.BEGIN);
       lookUp.put("div", TokenType.DIV);
       lookUp.put("do", TokenType.DO);
       lookUp.put("else", TokenType.ELSE);
       lookUp.put("end", TokenType.END);
       lookUp.put("function", TokenType.FUNCTION);
       lookUp.put("if", TokenType.IF);
       lookUp.put("integer", TokenType.INTEGER);
       lookUp.put("mod", TokenType.MOD);
       lookUp.put("not", TokenType.NOT);
       lookUp.put("of", TokenType.OF);
       lookUp.put("or", TokenType.OR);
       lookUp.put("procedure", TokenType.PROCEDURE);
       lookUp.put("program", TokenType.PROGRAM);
       lookUp.put("real", TokenType.REAL);
       lookUp.put("then", TokenType.THEN);
       lookUp.put("var", TokenType.VAR);
       lookUp.put("while", TokenType.WHILE);
       lookUp.put(";", TokenType.SEMICOLON);
       lookUp.put(",", TokenType.COMMA);
       lookUp.put(".", TokenType.PERIOD);
       lookUp.put(":", TokenType.COLON);
       lookUp.put("[", TokenType.LEFTSQUAREBRACKET);
       lookUp.put("]", TokenType.RIGHTSQUAREBRACKET);
       lookUp.put("(", TokenType.LEFTPARANTHESIS);
       lookUp.put(")", TokenType.RIGHTPARANTHESIS);
       lookUp.put("+", TokenType.PLUS);
       lookUp.put("-", TokenType.MINUS);
       lookUp.put("=", TokenType.EQUALS);
       lookUp.put("<>", TokenType.DIAMOND);
       lookUp.put("<", TokenType.LESSTHAN);
       lookUp.put(">", TokenType.GREATERTHAN);
       lookUp.put("<=", TokenType.LESSTHANEQUALS);
       lookUp.put(">=", TokenType.GREATERTHANEQUALS);
       lookUp.put("*",TokenType.ASTERISK);
       lookUp.put("/", TokenType.FORWARDSLASH);
       lookUp.put(":=", TokenType.COLONEQUALS);
   }

   
}
