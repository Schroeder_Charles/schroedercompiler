package scanner;
/**
 * This class initializes Token objects which store info about the tokens.
 * @author Charlie Schroeder
 */
public class Token
{
	private String contents;
        private TokenType type;
        private int value;
	/**
         * This overloaded function differentiates between variables, integers, and the EOF token
         * @param input 
         */
	public Token(String input)
	{
		this.contents = input;
                this.type = LookUpTable.lookUp.get(input);
                
                if(this.type == null)
                {
                    type = TokenType.ID;
                }
                
                System.out.println("type: " + this.type);
                
	};
        
        public Token(int input, String stringForm)
        {
            this.contents = stringForm;
            this.value = input;
            type = TokenType.NUM;
        }
        
        public Token(String stringForm, Object shouldBeNull)
        {
            this.contents = stringForm;
            type = TokenType.EOF;
        }
        
        
        public TokenType getType()
        {
            return type;
        }
        
	
   public String getLexeme() {return this.contents;}
   
   public int getValue() {return this.value;}
    
    @Override
    public String toString() {return this.contents;}
}
