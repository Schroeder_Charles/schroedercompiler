package scanner;
/*
 * This class is initializes the file reader, lookup table, and scanner. 
 * The lex token starts the scanning process. 
 * This class was initially used to run the scanner independent of the parser.
 * It is now obselete except for the aforementioned purpose.
 */


import java.io.FileInputStream;
import java.io.InputStreamReader;
/**
 *
 * @author Charlie Schroeder
 * the args string takes in the file name
 */
public class main {

   public static void main( String[] args) 
    {
       
        String filename = args[0];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream( filename);
        } catch (Exception e ) { e.printStackTrace();}
        InputStreamReader isr = new InputStreamReader( fis);
        ScannerJFlex scanner = new ScannerJFlex( isr);
        
        Token lex = null;
        
        do
        {
        	try {
        			lex = scanner.nextToken();
        		}
        		catch(Exception e) {e.printStackTrace();}
        		
        		
        } while (lex != null);
    }
    
}
