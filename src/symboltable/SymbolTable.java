/*
 * This class handles the creation and storage of symbols into a table which
 * will be accessed by the parser and the semantic analyzer. The symbols are 
 * divided into categories based on their "kind." Symbols consist of IDs specified
 * by the author of the file to be compiled; e.g. "program foo" would be stored as
 * the name string Foo, and the kind program_name.
 */
package symboltable;

import java.util.HashMap;
import scanner.LookUpTable;
import syntaxtree.TypeEnum;


/**
 *
 * @author Charlie Schroeder
 */
public class SymbolTable {
    
    private HashMap<String, Info> table = new HashMap();
    
    private class Info
    {
        String name;
        IdKind kind;
        TypeEnum type;
        //idKind is the enum
    }
    
    public enum IdKind {
        PROGRAM_NAME, PROCEDURE_NAME, VARIABLE_NAME, FUNCTION_NAME,
        //TODO type array
    }
    
    /**
     * This function adds a symbol to the table. Call this function with the string name
     * and the type.
     * @param name
     * @param kind 
     */
    public void add(String name, IdKind kind)
    {
        if(exists(name))
        {
          //do not add duplicates    
        }
        else
        {
            if(checkGoodVarName(name))
            {
                Info symbol = new Info();
                symbol.name = name;
                symbol.kind = kind;
                table.put(name, symbol);
            }
            else
            {
                System.out.println("Illegal var name: " + name + "\n");
            }
        }
    }
    
    
    /**
     * This function takes in the user defined string and returns what kind of 
     * symbol it is.
     * @param name
     * @return 
     */
    public IdKind findKind(String name)
    {
        Info temp;
        temp = new Info();
        temp.name = name;
        temp = table.get(name);
        return temp.kind;
    }
    
    /**
     * This function checks to see if a sting name is already entered into the table
     * Multiple string names cannot be entered more than once.
     * @param name
     * @return 
     */
    public boolean exists(String name)
    {
        boolean doesExist;
        doesExist = table.containsKey(name);
        return doesExist;
    }
    
    /**
     * This function outputs the key name and what type of token it is for all the keys
     * in the table.
     */
    public void outputST ()
    {
        for(String key : table.keySet())
        {
            System.out.println("Key:" + key + " Kind: " + findKind(key));
        }
    }
    
    public void setType(String name, TypeEnum type)
    {
        Info i = table.get(name);
        if(i != null)
        {
            i.type = type;
        }
    }
    
    public TypeEnum getType(String name)
    {
        Info i = table.get(name);
        return i.type;
    }
    /**
     * This function compares the entered function name to the list of keywords and symbols.
     * If the name is found, this function returns false, as in, that is not a good var name.
     * Subsequently, an error is thrown.
     * @param varName
     * @return 
     */
    private boolean checkGoodVarName(String varName)
    {
        if(LookUpTable.lookUp.containsKey(varName))
        {
            return false;
        } else
        {
            return true;
        }
    }
    
}
