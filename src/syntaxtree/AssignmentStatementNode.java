
package syntaxtree;

/**
 * Represents a single assignment statement.
 * @author Charlie Schroeder
 */
public class AssignmentStatementNode extends StatementNode {
    
    private VariableNode lvalue;
    private ExpressionNode expression;
    private int arrayAssignmentIndex = -1;

    public VariableNode getLvalue() {
        return lvalue;
    }

    public void setLvalue(VariableNode lvalue) {
        this.lvalue = lvalue;
    }

    public ExpressionNode getExpression() {
        return expression;
    }

    public void setExpression(ExpressionNode expression) {
        this.expression = expression;
    }
    

    
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Assignment\n";
        answer += this.lvalue.indentedToString( level + 1);
        answer += this.expression.indentedToString( level + 1);
        return answer;
    }
    /**
     * The array Assignment Index keeps track of where in the array the assignment 
     * statement is attempting to reassign.
     * @param arrayAssignmentIndex 
     */
    public void setArrayAssignmentIndex(int arrayAssignmentIndex) {
        this.arrayAssignmentIndex = arrayAssignmentIndex;
    }
    
    public int getArrayAssignmentIndex() {
        return this.arrayAssignmentIndex;
    }
}
