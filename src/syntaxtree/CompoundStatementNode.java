
package syntaxtree;

import java.util.ArrayList;

/**
 * Represents a compound statement in Pascal.
 * A compound statement is a block of zero or more
 * statements to be run sequentially.
 * @author Charlie Schroeder
 */
public class CompoundStatementNode extends StatementNode {
    
    private ArrayList<StatementNode> statements = new ArrayList<StatementNode>();
    
    public void addStatement( StatementNode state) {
        this.statements.add( state);
    }
    
    public ArrayList getStatements() {
        return statements;
    }
    
    public String indentedToString( int level) {
        
        String answer = this.indentation( level);
        answer += "Compound Statement\n";
        for( StatementNode state : statements) {
            if(state != null){answer += state.indentedToString( level + 1);}
        }
        return answer;
    }
}
