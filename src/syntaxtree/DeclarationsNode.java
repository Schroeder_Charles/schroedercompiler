
package syntaxtree;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Represents a set of declarations in a Pascal program.
 * @author Charlie Schroeder
 */
public class DeclarationsNode extends SyntaxTreeNode {
    
    private ArrayList<VariableNode> vars = new ArrayList<VariableNode>();
    
    public void addVariable( VariableNode aVariable) {
        vars.add( aVariable);
    }
    
    public ArrayList getVariables()
    {
        return vars;
    }
    
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Declarations: \n";
        for( VariableNode variable : vars) {
            answer += variable.indentedToString( level + 1);
        }
        return answer;
    }
    
    public Iterator<VariableNode> variables() {
        return vars.iterator();
    }
}
