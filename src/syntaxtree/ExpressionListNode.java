/*
 * This class simply stores a list of expression nodes.
 */
package syntaxtree;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class stores expression nodes which are used for many different
 * statements.
 * @author Charlie
 */
public abstract class ExpressionListNode extends SyntaxTreeNode{
    
    private ArrayList<ExpressionNode> vars = new ArrayList<ExpressionNode>();
    
    public void addVariable( ExpressionNode anExpression) {
        vars.add( anExpression);
    }
    
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        for( ExpressionNode variable : vars) {
            answer += variable.indentedToString( level + 1);
        }
        return answer;
    }
    
    public Iterator<ExpressionNode> variables() {
        return vars.iterator();
    }
}
