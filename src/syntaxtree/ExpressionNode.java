package syntaxtree;

/**
 * General representation of any expression.
 * @author Charlie Schroeder
 */
public abstract class ExpressionNode extends SyntaxTreeNode {
    
    private TypeEnum type;

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }
    
    
    
}
