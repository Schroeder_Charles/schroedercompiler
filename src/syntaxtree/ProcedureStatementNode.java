/*
 * Procedure Statements are functions that do not return anything. This class
 * handles storing information about them.
 */
package syntaxtree;

import java.util.ArrayList;

/**
 * This node is used to store all the various pieces of a procedure statement.
 * @author Charlie Schroeder
 */
public class ProcedureStatementNode extends StatementNode {

    private String name;
    private ArrayList<ExpressionNode> arguments;
    
    public void setName(String name)
    {
        this.name = name;
    }

    public ArrayList<ExpressionNode> getArguments() {
        return this.arguments;
    }

    public void setArguments(ArrayList<ExpressionNode> arguments) {
        this.arguments = arguments;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    
    @Override
    public String indentedToString(int level) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
}
