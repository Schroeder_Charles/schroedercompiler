/*
 * This class is for function and procedure delcations and storing the relevant info.
 */
package syntaxtree;

/**
 * Use this class to store all of the delcarations, compoundstatements, and any
 * other necessary information for functions and procedures. 
 * @author Charlie Schroeder
 */
public class SubProgramNode extends SyntaxTreeNode{
    
    private DeclarationsNode dn;
    private SubProgramDeclarationsNode sp;
    private CompoundStatementNode csn;
    private TypeEnum returnType; //if return type == null, then it's a procedure
    private String functionName;
    
    
    public SubProgramNode(DeclarationsNode dn, SubProgramDeclarationsNode sp, CompoundStatementNode csn )
    {
        this.dn = dn;
        this.sp = sp;
        this.csn = csn;
    }
    
    public SubProgramNode()
    {
        
    }
    
    public DeclarationsNode getDn() {
        return dn;
    }

    public void setDn(DeclarationsNode dn) {
        this.dn = dn;
    }

    public SubProgramDeclarationsNode getSp() {
        return sp;
    }

    public void setSp(SubProgramDeclarationsNode sp) {
        this.sp = sp;
    }

    public CompoundStatementNode getCsn() {
        return csn;
    }

    public void setCsn(CompoundStatementNode csn) {
        this.csn = csn;
    } 
    
    public void setFuncName(String name) {
        this.functionName = name;
    }
    
    public String getFuncName() {
        return functionName;
    }
    
    public void setReturnType(TypeEnum type) {
        this.returnType = type;
    }
    
    public TypeEnum getReturnType() {
        return this.returnType;
    }

    @Override
    public String indentedToString(int level) {
        String answer = this.indentation( level);
        answer += "SubProgramNode: " + this.functionName + "\n";
        answer += this.dn.indentedToString( level + 1);
        answer += this.sp.indentedToString( level + 1);
        answer += this.csn.indentedToString( level + 1);
        return answer;
    }
    
}
