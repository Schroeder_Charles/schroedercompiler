/*
 * Holds information about While statements.
 */
package syntaxtree;

/**
 * This class extends the statementnode class but also stores some additional information
 * necessary for the proper generation of while statement code.
 * @author Charlie Schroeder
 */
public class WhileNode extends StatementNode {
    
    private ExpressionNode condition;
    private StatementNode doNode;
    
    public ExpressionNode getCondition()
    {
        return this.condition;
    }
    
    public void setCondition(ExpressionNode eNode)
    {
        this.condition = eNode;
    }
    
    public void setDo(StatementNode sNode)
    {
        this.doNode = sNode;
    }
    
    public StatementNode getDo()
    {
        return this.doNode;
    }
    
    @Override
    public String indentedToString(int level)
    {
        String answer = this.indentation( level);
        answer += "While\n";
        answer += this.condition.indentedToString( level + 1);
        answer += this.doNode.indentedToString( level + 1);
        return answer;
    }
}
