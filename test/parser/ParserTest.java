/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import scanner.TokenType;
import syntaxtree.CompoundStatementNode;
import syntaxtree.DeclarationsNode;
import syntaxtree.ExpressionNode;
import syntaxtree.OperationNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.SubProgramDeclarationsNode;
import syntaxtree.SubProgramNode;
import syntaxtree.TypeEnum;
import syntaxtree.VariableNode;

/**
 *
 * @author Charlie
 */
public class ParserTest {
    
    public ParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of match method, of class Parser.
     */
    @Test
    public void testMatch() {
        System.out.println("match");
        TokenType expected = null;
        Parser instance = null;
        instance.match(expected);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of error method, of class Parser.
     */
    @Test
    public void testError() {
        System.out.println("error");
        String message = "";
        Parser instance = null;
        instance.error(message);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of program method, of class Parser.
     */
    @Test
    public void testProgram() {
        System.out.println("program");
        Parser instance = null;
        ProgramNode expResult = null;
        ProgramNode result = instance.program();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of identifier_list method, of class Parser.
     */
    @Test
    public void testIdentifier_list() {
        System.out.println("identifier_list");
        DeclarationsNode dn = null;
        Parser instance = null;
        DeclarationsNode expResult = null;
        DeclarationsNode result = instance.identifier_list(dn);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of declarations method, of class Parser.
     */
    @Test
    public void testDeclarations() {
        System.out.println("declarations");
        DeclarationsNode dn = null;
        Parser instance = null;
        DeclarationsNode expResult = null;
        DeclarationsNode result = instance.declarations(dn);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of type method, of class Parser.
     */
    @Test
    public void testType() {
        System.out.println("type");
        Parser instance = null;
        TypeEnum expResult = null;
        TypeEnum result = instance.type();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of standard_type method, of class Parser.
     */
    @Test
    public void testStandard_type() {
        System.out.println("standard_type");
        Parser instance = null;
        TypeEnum expResult = null;
        TypeEnum result = instance.standard_type();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of subprogram_declarations method, of class Parser.
     */
    @Test
    public void testSubprogram_declarations() {
        System.out.println("subprogram_declarations");
        SubProgramDeclarationsNode spdn = null;
        DeclarationsNode dn = null;
        CompoundStatementNode csn = null;
        Parser instance = null;
        SubProgramDeclarationsNode expResult = null;
        SubProgramDeclarationsNode result = instance.subprogram_declarations(spdn, dn, csn);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of subprogram_declaration method, of class Parser.
     */
    @Test
    public void testSubprogram_declaration() {
        System.out.println("subprogram_declaration");
        SubProgramDeclarationsNode spdn = null;
        DeclarationsNode dn = null;
        CompoundStatementNode csn = null;
        Parser instance = null;
        SubProgramNode expResult = null;
        SubProgramNode result = instance.subprogram_declaration(spdn, dn, csn);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of subprogram_head method, of class Parser.
     */
    @Test
    public void testSubprogram_head() {
        System.out.println("subprogram_head");
        DeclarationsNode dn = null;
        Parser instance = null;
        instance.subprogram_head(dn);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of arguments method, of class Parser.
     */
    @Test
    public void testArguments() {
        System.out.println("arguments");
        DeclarationsNode dn = null;
        Parser instance = null;
        instance.arguments(dn);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of parameter_list method, of class Parser.
     */
    @Test
    public void testParameter_list() {
        System.out.println("parameter_list");
        DeclarationsNode dn = null;
        Parser instance = null;
        instance.parameter_list(dn);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of compound_statement method, of class Parser.
     */
    @Test
    public void testCompound_statement() {
        System.out.println("compound_statement");
        CompoundStatementNode csn = null;
        Parser instance = null;
        CompoundStatementNode expResult = null;
        CompoundStatementNode result = instance.compound_statement(csn);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of optional_statements method, of class Parser.
     */
    @Test
    public void testOptional_statements() {
        System.out.println("optional_statements");
        CompoundStatementNode csn = null;
        Parser instance = null;
        CompoundStatementNode expResult = null;
        CompoundStatementNode result = instance.optional_statements(csn);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of statement_list method, of class Parser.
     */
    @Test
    public void testStatement_list() {
        System.out.println("statement_list");
        CompoundStatementNode csn = null;
        Parser instance = null;
        CompoundStatementNode expResult = null;
        CompoundStatementNode result = instance.statement_list(csn);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of statement method, of class Parser.
     */
    @Test
    public void testStatement() {
        System.out.println("statement");
        CompoundStatementNode csn = null;
        Parser instance = null;
        StatementNode expResult = null;
        StatementNode result = instance.statement(csn);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of variable method, of class Parser.
     */
    @Test
    public void testVariable() {
        System.out.println("variable");
        Parser instance = null;
        VariableNode expResult = null;
        VariableNode result = instance.variable();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of procedure_statement method, of class Parser.
     */
    @Test
    public void testProcedure_statement() {
        System.out.println("procedure_statement");
        Parser instance = null;
        instance.procedure_statement();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of expression_list method, of class Parser.
     */
    @Test
    public void testExpression_list() {
        System.out.println("expression_list");
        Parser instance = null;
        ArrayList expResult = null;
        ArrayList result = instance.expression_list();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of expression method, of class Parser.
     */
    @Test
    public void testExpression() {
        System.out.println("expression");
        Parser instance = null;
        ExpressionNode expResult = null;
        ExpressionNode result = instance.expression();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of simple_expression method, of class Parser.
     */
    @Test
    public void testSimple_expression() {
        System.out.println("simple_expression");
        Parser instance = null;
        ExpressionNode expResult = null;
        ExpressionNode result = instance.simple_expression();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of simple_part method, of class Parser.
     */
    @Test
    public void testSimple_part() {
        System.out.println("simple_part");
        Parser instance = null;
        ExpressionNode expResult = null;
        ExpressionNode result = instance.simple_part();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of term method, of class Parser.
     */
    @Test
    public void testTerm() {
        System.out.println("term");
        Parser instance = null;
        ExpressionNode expResult = null;
        ExpressionNode result = instance.term();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of term_part method, of class Parser.
     */
    @Test
    public void testTerm_part() {
        System.out.println("term_part");
        Parser instance = null;
        ExpressionNode expResult = null;
        ExpressionNode result = instance.term_part();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of factor method, of class Parser.
     */
    @Test
    public void testFactor() {
        System.out.println("factor");
        Parser instance = new Parser("this", false);
        ExpressionNode en = new VariableNode("this");
        ExpressionNode expResult = en;
        ExpressionNode result = instance.factor();
        assertEquals(expResult, result);
    }

    /**
     * Test of sign method, of class Parser.
     */
    @Test
    public void testSign() {
        System.out.println("sign");
        Parser instance = null;
        OperationNode expResult = null;
        OperationNode result = instance.sign();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of array method, of class Parser.
     */
    @Test
    public void testArray() {
        System.out.println("array");
        Parser instance = null;
        instance.array();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of mulop method, of class Parser.
     */
    @Test
    public void testMulop() {
        System.out.println("mulop");
        boolean isMulop = false;
        Parser instance = null;
        boolean expResult = false;
        boolean result = instance.mulop(isMulop);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of relop method, of class Parser.
     */
    @Test
    public void testRelop() {
        System.out.println("relop");
        Parser instance = null;
        boolean expResult = false;
        boolean result = instance.relop();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSymbolTable method, of class Parser.
     */
    @Test
    public void testGetSymbolTable() {
        System.out.println("getSymbolTable");
        Parser instance = null;
        instance.getSymbolTable();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
