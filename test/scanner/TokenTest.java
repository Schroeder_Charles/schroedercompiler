/*
 * This tests the Token class.
 */
package scanner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Charlie
 */
public class TokenTest {
    
    public TokenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        LookUpTable.initialize();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLexeme method, of class Token.
     */
    @Test
    public void testGetLexeme() {
        System.out.println("getLexeme");
        Token instance = new Token( "and");
        String expResult = "and";
        String result = instance.getLexeme();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Token.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Token instance = new Token("and", TokenType.AND);
        String expResult = "and";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getType method, of class Token.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Token instance = new Token("and", TokenType.AND);
        TokenType expResult = TokenType.AND;
        TokenType result = instance.getType();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
