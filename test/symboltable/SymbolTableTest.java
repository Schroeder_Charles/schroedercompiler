/*
 * This class tests the SymbolTable class; the tests below ensure the proper
 * creation and storage of symbols.
 */
package symboltable;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import symboltable.SymbolTable.IdKind;

/**
 *
 * @author Charlie
 */
public class SymbolTableTest {
    
    SymbolTable st;
    
    public SymbolTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        st = new SymbolTable();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class SymbolTable.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        String name = "foo";
        SymbolTable.IdKind kind = IdKind.PROGRAM_NAME;
        st.add(name, kind);
    }
    @Test 
    public void testFindKind() {
        System.out.println("testing findKind");
        String name = "foo";
        SymbolTable.IdKind kind = IdKind.PROCEDURE_NAME;
        st.add(name, kind);
        IdKind expected = IdKind.PROCEDURE_NAME;
        IdKind actual;
        actual = st.findKind(name);
        assertEquals(expected, actual);
    }
    
    @Test
    public void testExists() {
        System.out.println("Testing exists");
        String name = "foo";
        SymbolTable.IdKind kind = IdKind.PROCEDURE_NAME;
        st.add(name, kind);
        boolean expected = true;
        boolean actual;
        actual = st.exists(name);
        assertEquals(expected, actual);
    }

    
}
